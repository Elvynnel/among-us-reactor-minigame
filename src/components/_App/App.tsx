import { ThemeProvider } from 'styled-components';
import { theme } from '../../utils/theme';
import { ReactorMinigame } from '../views/ReactorMinigame/ReactorMinigame';
import { GlobalStyle } from '../_styles/globalStyles';
import './App.css';

function App() {
    return (
        <div className="App" data-testid="App">
            <ThemeProvider theme={theme}>
                <GlobalStyle />
                <ReactorMinigame />
            </ThemeProvider>
        </div>
    );
}

export default App;
