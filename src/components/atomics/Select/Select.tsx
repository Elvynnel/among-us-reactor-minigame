import React, { HTMLProps } from 'react';
import styled, { css } from 'styled-components';
import { v4 as uuid } from 'uuid';

interface IProps extends HTMLProps<HTMLSelectElement> {
    options: { value: string | number; label: string }[];
    label?: string;
}

export const Select = ({
    options = [],
    label = '',
    onChange,
    value,
    disabled,
}: IProps) => {
    const htmlId = uuid();

    return (
        <Wrapper>
            <StyledLabel htmlFor={htmlId}>{label}</StyledLabel>
            <StyledSelect
                name="pets"
                id={htmlId}
                onChange={onChange}
                value={value}
                disabled={disabled}
            >
                {options.map(({ value, label }) => (
                    <option key={uuid()} value={value}>
                        {label}
                    </option>
                ))}
            </StyledSelect>
        </Wrapper>
    );
};

const Wrapper = styled.div(
    () => css`
        display: grid;
        grid-template-columns: repeat(2, min-content);
        align-items: center;
    `,
);

const StyledSelect = styled.select(
    ({ theme: { spacing } }) =>
        css`
            height: 100%;
            min-height: ${spacing.l}rem;
            margin-left: ${spacing.xs}rem;
        `,
);

const StyledLabel = styled.label(
    ({ theme: { fontSize } }) =>
        css`
            font-size: ${fontSize.l}rem;
        `,
);
