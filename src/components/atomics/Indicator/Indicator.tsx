import styled, { css } from 'styled-components';

interface IProps {
    isActive?: boolean;
}

export const Indicator = ({ isActive }: IProps) => {
    return <Wrapper isActive={isActive} />;
};

const Wrapper = styled.div<{ isActive: boolean }>(
    ({ theme: { colors, spacing }, isActive }) => css`
        width: ${spacing.m}rem;
        height: ${spacing.m}rem;
        border-radius: 50%;
        border: 1px ${colors.borderSecondary} solid;
        background: ${isActive
            ? `linear-gradient(to bottom right, ${colors.positive} 0%, ${colors.success} 100%)`
            : colors.backgroundPrimary};
    `,
);
