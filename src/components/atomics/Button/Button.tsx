import { HTMLProps, ReactNode } from 'react';
import styled, { css } from 'styled-components';

type TButtonVariant = 'default' | 'panel';

type TButtonColorVariant = 'positive' | 'neutral';
interface IProps extends HTMLProps<HTMLButtonElement> {
    children?: ReactNode;
    variant?: TButtonVariant;
    colorVariant?: TButtonColorVariant;
}

export const Button = ({
    children,
    onClick,
    disabled,
    variant = 'default',
    colorVariant,
}: IProps) => {
    return (
        <StyledButton
            onClick={onClick}
            disabled={disabled}
            variant={variant}
            colorVariant={colorVariant}
        >
            {children}
        </StyledButton>
    );
};

const StyledButton = styled.button<{
    variant: TButtonVariant;
    colorVariant?: TButtonColorVariant;
}>(
    ({ theme: { colors, spacing, fontWeight }, variant, colorVariant }) => css`
        outline: none;
        font-weight: ${fontWeight.bold};

        &:hover {
            opacity: 0.7;
            transition: all 0.4s ease 0s;
        }

        &:disabled {
            opacity: 0.7;
        }

        ${variant === 'default'
            ? css`
                  min-height: ${spacing.l}rem;
                  min-width: ${spacing.xxl}rem;
                  border-radius: 3px;
              `
            : css`
                  padding: 0;
                  border-radius: 1px;
                  border: none;
              `}

        ${colorVariant === 'positive' &&
        css`
            background-color: ${colors.positive};
            border: 2px ${colors.borderPositive} solid;
            color: ${colors.textSecondary};
        `}

        ${colorVariant === 'neutral' &&
        css`
            background-color: ${colors.neutral};
            border: 2px ${colors.borderNeutral} solid;
            color: ${colors.textSecondary};
        `}
    `,
);
