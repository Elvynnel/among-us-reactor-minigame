import React, { HTMLProps, ReactNode } from 'react';
import styled, { css } from 'styled-components';
import { v4 as uuid } from 'uuid';
import { Indicator } from '../../atomics/Indicator/Indicator';

interface IProps extends HTMLProps<HTMLButtonElement> {
    children?: ReactNode;
    indicatorsAmount?: number;
    activeIndicatorsAmount?: number;
}

export const Panel = ({
    children,
    indicatorsAmount = 0,
    activeIndicatorsAmount = 0,
}: IProps) => {
    return (
        <Wrapper>
            <IndicatorsWrapper>
                {[...Array(indicatorsAmount).keys()].map((value) => (
                    <Indicator
                        key={uuid()}
                        isActive={value < activeIndicatorsAmount}
                    />
                ))}
            </IndicatorsWrapper>
            <Content>{children}</Content>
        </Wrapper>
    );
};

const Wrapper = styled.div(
    ({ theme: { colors, spacing } }) => css`
        display: block;
        padding: 0rem ${spacing.s}rem ${spacing.s}rem;
        background-color: ${colors.border};
    `,
);

const IndicatorsWrapper = styled.div(
    ({ theme: { spacing } }) => css`
        display: grid;
        grid-auto-flow: column;
        justify-items: center;
        padding: ${spacing.xs}rem 0;
    `,
);

const Content = styled.div(
    ({ theme: { colors, spacing } }) => css`
        position: relative;
        width: 100%;
        display: grid;
        grid-template-columns: repeat(4, minmax(2.5rem, 3.5rem));
        grid-template-rows: repeat(4, minmax(2.5rem, 3.5rem));
        grid-gap: ${spacing.xs}rem;
        background-color: ${colors.backgroundSecondary};
        border: 1px ${colors.borderSecondary} solid;
    `,
);
