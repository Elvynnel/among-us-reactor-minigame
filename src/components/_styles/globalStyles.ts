import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
    }

    *, *:before, *:after {
        -webkit-box-sizing: inherit;
        -moz-box-sizing: inherit;
        box-sizing: inherit;
    }

    html {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        height: 100%;
        ${({ theme: { fontSize, lineHeight } }) => `
            font-size: clamp(${fontSize.s}rem, ${fontSize.s}rem + 0.5vw, ${fontSize.m}rem);
            line-height: clamp(${lineHeight.l}rem, ${lineHeight.l}rem + 0.5vw, ${lineHeight.xl}rem);
        `};

    }

    body {
        ${({ theme: { colors, typographies } }) => `
        color: ${colors.textPrimary};
        font-family: ${typographies.default}, ${typographies.prominent};

        /* Override default button highlight */
        -webkit-tap-highlight-color: ${colors.buttonActive}20;
        -webkit-touch-callout: ${colors.buttonActive}20;
        -webkit-user-select: ${colors.buttonActive}20;
        -khtml-user-select: ${colors.buttonActive}20;
        -moz-user-select: ${colors.buttonActive}20;
        -ms-user-select: ${colors.buttonActive}20;
        user-select: ${colors.buttonActive}20;
        `};
    }
`;
