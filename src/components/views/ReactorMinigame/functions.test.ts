import { isInteger } from 'utils/functions/numbers';
import { generateNaturalNumbers } from './functions';

describe('ReactorGame helper functions', () => {
    // Number of test iterations is caused by randomness (non-pureness) nature of the function
    describe('generateIntegersArray', () => {
        const threshold = 50;
        const generatedNumbers = generateNaturalNumbers(
            10000,
            threshold,
        ) as number[];

        it('should return amount of elements equal to the argument passed', () => {
            expect(generatedNumbers.length === 10000).toBe(true);
        });

        it('should return only integers', () => {
            expect(generatedNumbers.every(isInteger)).toBe(true);
        });

        it('should return only values from 0 to threshold argument', () => {
            expect(
                generatedNumbers.every(
                    (value) => value >= 0 && value <= threshold,
                ),
            ).toBe(true);
        });

        it('should throw error when non-positive integer passed as amount arg (1)', () => {
            expect(() => {
                generateNaturalNumbers(-10);
            }).toThrow(new TypeError('Amount should be a positive integer'));
        });

        it('should throw error when non-positive integer passed as threshold arg (2)', () => {
            expect(() => {
                generateNaturalNumbers(10, -5);
            }).toThrow(new TypeError('Threshold should be a positive integer'));
        });
    });
});
