import React, { useEffect, useState } from 'react';
import styled, { css } from 'styled-components';
import { v4 as uuid } from 'uuid';
import { Button } from '../../atomics/Button/Button';
import { Select } from '../../atomics/Select/Select';
import { Panel } from '../../organisms/Panel/Panel';
import { applyStyle } from '../../_styles/applyStyle';
import { generateNaturalNumbers } from './functions';

export const ReactorMinigame = () => {
    const [targetSelections, setTargetSelections] = useState([]);
    const [recentlySelected, setRecentlySelected] = useState([]);
    const [correctlySelected, setCorrectlySelected] = useState([]);
    const [isShowingSelected, setIsShowingSelected] = useState(false);
    const [amountOfStepsToWin, setAmountOfStepsToWin] = useState(5);
    const [squareToShow, setSquareToShow] = useState(-1);

    const showingDurationInMs = 500;
    const showingPauseInMs = 100;
    const isGameRunning = targetSelections.length !== 0;
    const isGameWon =
        targetSelections.length !== 0 &&
        correctlySelected.length === targetSelections.length;

    const showCombination = () => {
        setIsShowingSelected(true);

        let interval = setInterval(
            (gen) => {
                const { value, done } = gen.next();

                if (done) {
                    clearInterval(interval);
                    setSquareToShow(-1);
                    setIsShowingSelected(false);
                } else {
                    setSquareToShow(value);
                    setTimeout(() => setSquareToShow(-1), showingDurationInMs);
                }
            },
            showingDurationInMs + showingPauseInMs,
            targetSelections
                .slice(0, correctlySelected.length + 1)
                [Symbol.iterator](),
        );
    };

    const checkLatestSelection = (value: number) => {
        if (targetSelections[recentlySelected.length] === value) {
            setRecentlySelected((prev) => [...prev, value]);
        } else {
            setRecentlySelected([]);
            showCombination();
        }
    };

    const onResetClick = () => {
        setAmountOfStepsToWin(5);
        setTargetSelections([]);
        setRecentlySelected([]);
        setCorrectlySelected([]);
    };

    const onStartClick = () => {
        setTargetSelections(
            generateNaturalNumbers(amountOfStepsToWin) as number[],
        );
    };

    useEffect(() => {
        if (recentlySelected.length > correctlySelected.length) {
            setCorrectlySelected(recentlySelected);
        }
    }, [recentlySelected]);

    useEffect(() => {
        setRecentlySelected([]);
        if (correctlySelected.length !== targetSelections.length) {
            showCombination();
        }
    }, [correctlySelected, targetSelections]);

    return (
        <Wrapper>
            <PanelsWrapper>
                <Panel
                    indicatorsAmount={targetSelections.length}
                    activeIndicatorsAmount={correctlySelected.length}
                >
                    {[...Array(16).keys()].map((value) => (
                        <Square
                            key={uuid()}
                            isVisible={value === squareToShow}
                        />
                    ))}

                    <SuccessTextWrapper>
                        <SuccessText isHidden={!isGameWon}>
                            Success!
                        </SuccessText>
                        <ReplayText isHidden={!isGameWon}>
                            to replay, reset and start when ready
                        </ReplayText>
                    </SuccessTextWrapper>
                </Panel>
                <Panel
                    indicatorsAmount={targetSelections.length}
                    activeIndicatorsAmount={recentlySelected.length}
                >
                    {[...Array(16).keys()].map((value) => (
                        <Button
                            key={uuid()}
                            onClick={() => {
                                checkLatestSelection(value);
                            }}
                            disabled={
                                isShowingSelected || !isGameRunning || isGameWon
                            }
                            variant="panel"
                        />
                    ))}
                </Panel>
            </PanelsWrapper>
            <SteeringWrapper>
                <Select
                    options={Array.from(
                        { length: 10 },
                        (_, index) => index + 1,
                    ).map((value) => ({
                        value,
                        label: String(value),
                    }))}
                    label="Steps:"
                    onChange={(e) => {
                        setAmountOfStepsToWin(e.target.value);
                    }}
                    value={amountOfStepsToWin}
                    disabled={isGameRunning}
                />
                <ButtonsWrapper>
                    <Button
                        onClick={onStartClick}
                        disabled={isGameRunning}
                        colorVariant="positive"
                    >
                        Start
                    </Button>
                    <Button
                        onClick={onResetClick}
                        disabled={!isGameRunning}
                        colorVariant="neutral"
                    >
                        Reset
                    </Button>
                </ButtonsWrapper>
            </SteeringWrapper>
        </Wrapper>
    );
};

const Wrapper = styled.div(() => css``);

const PanelsWrapper = styled.div(
    ({ theme: { spacing } }) => css`
        display: grid;
        grid-template:
            '1fr'
            '1fr';
        grid-gap: ${spacing.m}rem;

        ${applyStyle.from.tablet} {
            grid-template: '1fr 1fr';
        }
    `,
);

const Square = styled.div<{ isVisible: boolean }>(
    ({ theme: { colors }, isVisible }) =>
        css`
            visibility: ${isVisible ? 'visible' : 'hidden'};
            background-color: ${colors.squareActive};
        `,
);

const SteeringWrapper = styled.div(
    ({ theme: { spacing } }) => css`
        display: flex;
        align-items: space-between;
        justify-content: space-between;

        width: 100%;
        padding: ${spacing.xs}rem 0;
    `,
);

const ButtonsWrapper = styled.div(
    ({ theme: { spacing } }) => css`
        display: grid;
        grid-template: '1fr 1fr';
        grid-gap: ${spacing.xs}rem;
    `,
);

const SuccessTextWrapper = styled.div(
    ({ isHidden }) => css`
        position: absolute;
        top: 30%;

        display: flex;
        flex-direction: column;

        width: 100%;
    `,
);

const SuccessText = styled.p<{ isHidden: boolean }>(
    ({ theme: { fontSize, colors }, isHidden }) => css`
        display: flex;
        justify-content: center;
        opacity: ${isHidden ? 0 : 1};

        font-size: ${fontSize.xxl}rem;
        color: ${colors.success};
        transition: all 1s ease 0s;
    `,
);

const ReplayText = styled.p<{ isHidden: boolean }>(
    ({ theme: { fontSize, colors }, isHidden }) => css`
        display: block;
        opacity: ${isHidden ? 0 : 1};

        font-size: ${fontSize.xs}rem;
        color: ${colors.textSecondary};
        transition: all 1s ease 1s;
    `,
);
