/**
 * Generate natural numbers. 0 is also treated as natural number.
 * @param amount - amount of natural numbers to be calculated (positive integer)
 * @param threshold - maximum value of single generated number (1 - infinity, positive integer)
 */
export const generateNaturalNumbers = (
    amount: number = 5,
    threshold: number = 15,
): number[] | void => {
    if (amount > 0 && threshold > 0) {
        return Array.from({ length: amount }, () =>
            Math.floor(Math.random() * Math.floor(threshold - 1)),
        );
    } else if (amount < 0) {
        throw new TypeError('Amount should be a positive integer');
    } else if (threshold < 0) {
        throw new TypeError('Threshold should be a positive integer');
    }
};
