const paletteDark = {
    white: '#fff',
    grayLight: '#d5d5d5',
    gray: '#7e8185',
    shark: '#282c34',
    black: '#000',
    blue: '#387bd9',
    blueDark: '#0250bd',
    green: '#47a847',
    greenDark: '#008200',
    red: '#ff0000',
    redDark: '#9c0000',
};

export const theme = {
    colors: {
        textPrimary: paletteDark.black,
        textSecondary: paletteDark.white,
        backgroundPrimary: paletteDark.shark,
        backgroundSecondary: paletteDark.black,
        border: paletteDark.grayLight,
        borderSecondary: paletteDark.gray,
        borderNeutral: paletteDark.blue,
        borderPositive: paletteDark.green,
        buttonActive: paletteDark.gray,

        success: paletteDark.green,
        squareActive: paletteDark.blue,
        positive: paletteDark.greenDark,
        neutral: paletteDark.blueDark,
    },
    typographies: {
        default: 'Georgia',
        prominent: 'Bebas Neue',
    },
    spacing: {
        xs: 0.384,
        s: 0.64,
        m: 1,
        l: 1.563,
        xl: 2.441,
        xxl: 3.815,
    },
    fontSize: {
        xxxxs: 0.384,
        xxxs: 0.512,
        xxs: 0.64,
        xs: 0.8,
        s: 1,
        m: 1.25,
        l: 1.563,
        xl: 1.953,
        xxl: 2.441,
        xxxl: 3.052,
    },
    lineHeight: {
        xxs: 0.563,
        xs: 0.75,
        s: 1,
        m: 1.333,
        l: 1.777,
        xl: 2.369,
        xxl: 3.157,
        xxxl: 4.209,
    },
    fontWeight: {
        thin: 300,
        normal: 400,
        bold: 700,
    },
    deviceSize: {
        xs: 0,
        sm: 576,
        md: 768,
        lg: 992,
        xl: 1200,
        xxl: 1600,
    },
};
