export const isInteger = (value: number) => Number.isInteger(value) === true;
